﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.Configuration.Json;

namespace HelloWorld
{
    public class Program
    {
     
        public static void Main(string[] args)
        {

            var builder = new ConfigurationBuilder()
                     .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            var message = configuration["Appsettings:Message"];

            Console.WriteLine(message);
            Console.Read();
        }
    }
}
